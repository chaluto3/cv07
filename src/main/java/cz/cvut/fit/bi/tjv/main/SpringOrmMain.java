package cz.cvut.fit.bi.tjv.main;

import cz.cvut.fit.bi.tjv.model.Customer;
import cz.cvut.fit.bi.tjv.model.Product;
import cz.cvut.fit.bi.tjv.service.CustomerService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import cz.cvut.fit.bi.tjv.service.ProductService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpringOrmMain {
    public static void main(String[] args) {

        //Create Spring application context
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

        //Get service from context. (service's dependency (ProductDAO) is autowired in ProductService)
        ProductService productService = ctx.getBean(ProductService.class);
        CustomerService customerService = ctx.getBean(CustomerService.class);

        //Do some data operation

        Customer janNovak = new Customer("Jan", "Novak");
        Customer petrCerny = new Customer("Petr", "Cerny");
        Customer josefMaly = new Customer("Josef", "Maly");

        customerService.add(janNovak);
        customerService.add(petrCerny);
        System.out.println("listAll customers: " + customerService.listAll());

        Product product1 = new Product(1, "Bulb");

        List<Product> productList = new ArrayList<>();
        productList.add(product1);

        productService.addAll(productList);

        System.out.println("listAll customers: " + customerService.listAll());

        System.out.println("listAll: " + productService.listAll());

        //Test transaction rollback (duplicated key)

        try {
            productService.addAll(Arrays.asList(new Product(3, "Book"), new Product(4, "Soap"), new Product(1, "Computer")));
        } catch (DataAccessException dataAccessException) {
        }

        //Test element list after rollback
        System.out.println("listAll: " + productService.listAll());

        ctx.close();

    }
}
